Integer {
  use foreign java/lang/Integer,

  toString = (int: Int): String -> Integer#toString(int),

  parse = (str: String): Int -> Integer#parseInt(str)
}

List {
  use foreign java/util/ArrayList,
  use foreign java/util/Iterator,

  type T[A] = { list: ArrayList[A] },

  new = [A](): T[A] -> { list = ArrayList#new() },

  of = [A](value: A): T[A] -> {
    let list = ArrayList#new()
    ArrayList#add(list, value)
    { list = list }
  },

  conj = [A](list: T[A], value: A): T[A] -> {
    let listCopy = ArrayList#new(list.list)
    ArrayList#add(listCopy, value)
     { list = listCopy }
  },

  get = [A](list: T[A], idx: Int): A -> ArrayList#get(list.list, idx),

  size = [A](list: T[A]): Int -> ArrayList#size(list.list),

  map = [A, B](list: T[A], mapper: (value: A) -> B): T[B] -> {
    let size = ArrayList#size(list.list)
    let newList = ArrayList#new(size)
    loop (let i = 0) ->
      if i < size {
        let item = get(list, i)
        ArrayList#add(newList, mapper(item))
        recur(i + 1)
      } else {
        { list = newList }
      }
  },

  filter = [A](list: T[A], filterer: (value: A) -> Boolean): T[A] -> {
    let size = ArrayList#size(list.list)
    let newList = ArrayList#new(size)
    loop (let i = 0) ->
      if i < size {
        let item = get(list, i)
        if (filterer(item)) ArrayList#add(newList, item)
        recur(i + 1)
      } else {
        { list = newList }
      }
  },

  reduce = [A, B](list: T[A], init: B, accumulator: (accum: B, value: A) -> B): B -> {
    let size = size(list)
    loop (let i = 0, let b = init) ->
      if i < size {
        let item = get(list, i)
        recur(i + 1, accumulator(b, item))
      } else {
        b
      }
  },
}

Map {
  use foreign java/util/HashMap,
  use foreign java/util/Set,
  use foreign java/util/Iterator,
  use foreign java/util/Map$Entry,

  type T[A, B] = { map: HashMap[A, B] },

  new = [A, B](): T[A, B] -> { map = HashMap#new() },

  of = [A, B](key: A, value: B): T[A, B] -> {
    let  map = new()
    HashMap#put(map.map, key, value)
    map
  },

  get = [A, B](map: T[A, B], key: A): B -> HashMap#get(map.map, key),

  size = [A, B](map: T[A, B]): Int -> HashMap#size(map.map),

  assoc = [A, B](map: T[A, B], key: A, value: B): T[A, B] -> {
    let newMap = HashMap#new(map.map)
    HashMap#put(newMap, key, value)
    { map = newMap }
  },

  dissoc = [A, B](map: T[A, B], key: A): T[A, B] -> {
    let newMap = HashMap#new(map.map)
    HashMap#remove(newMap, key)
    { map = newMap }
  },

  map = [A, B, C, D](map: T[A, B], mapper: (entry: (A, B)) -> (C, D)): T[A, B] -> {
    let newMap = HashMap#new(HashMap#size(map.map))
    let entrySet = HashMap#entrySet(map.map)
    loop (let iter = Set#iterator(entrySet)) ->
      if Iterator#hasNext(iter) {
        let entry = Iterator#next(iter)
        HashMap#put(newMap, Map$Entry#getKey(entry), Map$Entry#getValue(entry))
        recur(iter)
      } else {
        { map = newMap }
      }
  },
}